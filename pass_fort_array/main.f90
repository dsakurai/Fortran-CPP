program main_fortran
    use iso_c_binding
    implicit none

    ! TODO change subroutine to function
    integer(c_size_t), allocatable :: fortran_array(:)
    integer :: i

    interface
        subroutine call_c(fort_array) bind(C, name="hello_cpp")
            import :: c_size_t
            integer(kind=c_size_t) :: fort_array(*) ! this '*' is important!
        end subroutine call_c
    end interface

    allocate (fortran_array(10))

    do i = 1, 10
        fortran_array(i) = i
        write(*,*) "fortran_array():", fortran_array(i)
    end do

    call call_c(fortran_array)
end program

