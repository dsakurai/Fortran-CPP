#include <iostream>

extern "C" int hello_cpp(size_t* fort_array) {
    std::cout << "Hello, World!" << std::endl;

    for (int i = 0; i < 10; ++i ){
        std::cout << "[C function] fort_array["<<i<<"]: "<<fort_array[i]<<std::endl;
    }

    return 0;
}
