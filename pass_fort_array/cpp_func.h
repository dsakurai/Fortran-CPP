//
// Created by bzfsakur on 7/14/17.
//

#ifndef FORTRAN_CPP_LIB_C_H
#define FORTRAN_CPP_LIB_C_H

extern "C"{
    extern int hello_cpp(size_t* fort_array);
}

#endif //FORTRAN_CPP_LIB_C_H
